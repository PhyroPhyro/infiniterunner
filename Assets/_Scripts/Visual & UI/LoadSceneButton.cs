﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneButton : MonoBehaviour {

    public string levelName;

	public void OnClick()
    {
        StartCoroutine(LoadSceneController.instance.LoadNextScene(levelName));
    }
}

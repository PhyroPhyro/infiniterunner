﻿using UnityEngine;

public class AnimateWaterUVs: MonoBehaviour
{
    public	Vector2	displacement1;
    public	Vector2	displacement2;
	public	float	baseAlpha;
	public	float	alphaRange;
	public	float	alphaTime;
	private float	_alpha = 1;

	private Material _material;

    private void Awake ()
    {
        _material = GetComponent<Renderer>().material;
    }

    private void LateUpdate ()
    {
		float t = Time.smoothDeltaTime;
		Vector2 currentOffset1 = _material.GetTextureOffset("_MainTex");
		Vector2 currentOffset2 = _material.GetTextureOffset("_BlendTex");

		Vector2 newOffset1 = currentOffset1 + displacement1 * t;
		Vector2 newOffset2 = currentOffset2 + displacement2 * t;

		_material.SetTextureOffset ("_MainTex", newOffset1);
		_material.SetTextureOffset ("_BlendTex", newOffset2);	}
}

﻿using UnityEngine;
using System.Collections;

public class FollowObject : MonoBehaviour {

    public GameObject objToFollow;
	void Update () {
        transform.position = objToFollow.transform.position;
	}
}

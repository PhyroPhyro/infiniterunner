﻿using UnityEngine;
using System.Collections;

public class ShareButton : MonoBehaviour {

	public void Share()
    {
        StartCoroutine(ShareScore());
    }

    IEnumerator ShareScore()
    {
        Application.CaptureScreenshot("share_screenshot");
        Texture2D shareTexture = null;

        while (true)
        {
            WWW www = new WWW("file://share_screenshot");
            yield return www;
            if (www.error == null)
                www.LoadImageIntoTexture(shareTexture);
            else
                break;
        }

        if (shareTexture != null)
            AndroidSocialGate.StartShareIntent("InfiniteRunner", "Look! I've got " + GameManager.instance.currentScore.ToString() + " on InfiniteRunner! This game is great! :)", shareTexture);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScenaryManager : MonoBehaviour {

    public static ScenaryManager instance;
    public List<GameObject> LevelScenaries;
    public List<int> LevelBoundaries;

    private GameObject currentScenary;
    private GameObject currentScenaryPrefab;

    void Start () {
        instance = this;
	}

    public IEnumerator SpawnNextScenary()
    { 
        StartCoroutine(GameManager.instance.PassScenary());

        yield return null;
        if(GameManager.instance.canSpawn)
        {
            Debug.Log("NEXT SCENARY");

            Vector3 refPosition = Vector3.zero;
            currentScenary = Instantiate(SelectNextScenary()) as GameObject;
            currentScenary.transform.position = new Vector3(refPosition.x, refPosition.y, 9300f);
        }
    }

    private GameObject SelectNextScenary()
    {
        int currentLevel = GameManager.instance.diffLevel;
        int randomScenaryIndex = Random.Range(currentLevel, currentLevel + 5);
        return LevelScenaries[randomScenaryIndex - 1];
    }

    public void SpawnSideScenary(GameObject refScenary)
    {
        Vector3 refPosition = refScenary.transform.position;
        GameObject sideObjLeft = Instantiate(currentScenary) as GameObject;
        GameObject sideObjRight = Instantiate(currentScenary) as GameObject;
        sideObjLeft.transform.position = new Vector3(-refPosition.x * 10, refPosition.y, refPosition.z);
        sideObjRight.transform.position = new Vector3(refPosition.x * 10, refPosition.y, refPosition.z);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneController : MonoBehaviour {

    public static LoadSceneController instance;
    public Image blackbox;
    public float fadingTime;

	void Start () {
        instance = this;
        DontDestroyOnLoad(gameObject);
	}
	
    public IEnumerator LoadNextScene(string sceneName)
    {
        yield return StartCoroutine(FadeBlackbox(1.0f, fadingTime));
        SceneManager.LoadSceneAsync(sceneName);
        yield return null;
    }

    void OnLevelWasLoaded(int scene)
    {
        blackbox.color = new Color(0f, 0f, 0f, 1f);
        StartCoroutine(FadeBlackbox(0.0f, fadingTime));
    }

    public IEnumerator FadeBlackbox(float aValue, float aTime)
    {
        float alpha = blackbox.color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(0, 0, 0, Mathf.Lerp(alpha, aValue, t));
            blackbox.color = newColor;
            yield return null;
        }
    }
}

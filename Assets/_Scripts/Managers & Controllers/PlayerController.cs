﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public Transform playerModel, playerCamera;
    public ParticleEmitter waterRailSteamL, waterRailSpitL, waterRailSteamR, waterRailSpitR;
    public float rotationSpeed;
    public float maxRotation;

    void Update()
    {
        if(GameManager.instance.isRunning)
        {

            float rotation = GameManager.instance.steerVelocity * -maxRotation;

            playerModel.localRotation = Quaternion.Lerp(playerModel.localRotation, new Quaternion(0,0,-rotation,1f), rotationSpeed);
            playerCamera.localRotation  = Quaternion.Lerp(playerCamera.localRotation, new Quaternion(0,0,-rotation/2,1f), rotationSpeed);

            if(GameManager.instance.steerVelocity >= 0.9f)
            {
                waterRailSteamL.emit = true;
                waterRailSpitL.emit = true;
            }
            else if(GameManager.instance.steerVelocity <= -0.9f)
            {
                waterRailSteamR.emit = true;
                waterRailSpitR.emit = true;
            }
            else
            {
                waterRailSteamL.emit = false;
                waterRailSpitL.emit = false;
                waterRailSteamR.emit = false;
                waterRailSpitR.emit = false;
            }
        }
    }
}

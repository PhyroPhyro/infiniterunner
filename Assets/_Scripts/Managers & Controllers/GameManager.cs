﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class GameManager : MonoBehaviour {

    public static GameManager instance;

    [Header("Speed & Steer")]
    public float steerVelocity;
    public float steerTime;
    public float maxTurning;
    public float maxSpeed;

    [Header("Conditions")]
    public bool isRunning = true;
    public bool canSpawn = true;

    [Header("Death")]
    public Camera deathCamera;
    public GameObject mobileControls, gameOverView, playerMisc, deathExplosion, UILines, waterTexture;

    [Header("Scenaries")]
    public int maxScenaries;
    public int currScenary = 0;
    public int diffLevel = 1;

    [Header("Score")]
    public float timeScoreModifier;
    public float nearMissScoreModifier;
    public int currentScore = 0;
    public int timeScore = 0;
    public int nearMissScore = 0;
    public int nearMissCounter = 0;

    void Start()
    {
        instance = this;
        StartCoroutine(StartGame());
    }

    void Update()
    {
        steerVelocity = Mathf.Lerp(steerVelocity, -CrossPlatformInputManager.GetAxis("Horizontal"), steerTime);

        if(isRunning)
        {
            timeScore += (int)(Time.deltaTime * timeScoreModifier);
            currentScore = timeScore + nearMissScore;
        }
    }

    IEnumerator StartGame()
    {
        StartCoroutine(ScenaryManager.instance.SpawnNextScenary());
        yield return null;
    }

    public IEnumerator PassScenary()
    {
        currScenary++;
        if(currScenary > maxScenaries)
        {
            diffLevel++;
            currScenary = 0;
            yield return StartCoroutine(StartNextLevel());
        }

        Debug.Log("PASS SCENARY");
    }

    IEnumerator StartNextLevel()
    {
        //Show next level graphic
        Debug.Log("START 5S");
        canSpawn = false;
        yield return new WaitForSeconds(5f);
        Debug.Log("5s PASSED");
        canSpawn = true;
        StartCoroutine(ScenaryManager.instance.SpawnNextScenary());
    }

    public void AddNearMiss()
    {
        if(isRunning)
        {
            nearMissCounter++;
            nearMissScore = (int)(nearMissCounter * nearMissScoreModifier);
        }
    }

    public void EndGame()
    {
        isRunning = false;
        Camera.main.gameObject.SetActive(false);
        deathCamera.gameObject.SetActive(true);
        deathExplosion.SetActive(true);
        playerMisc.SetActive(false);
        mobileControls.SetActive(false);

        UILines.SetActive(false);
        waterTexture.GetComponent<AnimateWaterUVs>().displacement2 = new Vector2(1f, 0);
        FindObjectOfType<PlayerController>().playerModel.gameObject.SetActive(false);
        StartCoroutine(EndGameRoutine());
    }

    IEnumerator EndGameRoutine()
    {        
        yield return new WaitForSeconds(5f);
        gameOverView.SetActive(true);
    }
}

﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIManager : MonoBehaviour {

    public static UIManager instance;

    public Text ScoreText;

    void Start()
    {
        instance = this;
    }

    void Update()
    {
        ScoreText.text = GameManager.instance.currentScore.ToString();
    }
}

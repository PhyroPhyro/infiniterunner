﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverViewController : MonoBehaviour {

    public Text scoreText, leaderboardText;
    public GameObject loadingLeaderboard, successLeaderboard, failLeaderboard;

    void Start()
    {
        scoreText.text = GameManager.instance.currentScore.ToString();
        AddScoreToLeaderboard(GameManager.instance.currentScore);
    }

    public void AddScoreToLeaderboard(float score)
    {
        GooglePlayManager.ActionScoreSubmited += OnScoreSubmited;
        GooglePlayManager.Instance.SubmitScore(GooglePlayInit.instance.leaderboard.Name, (long)score);
    }

    private void OnScoreSubmited(GP_LeaderboardResult result)
    {
        GooglePlayManager.ActionScoreSubmited -= OnScoreSubmited;
        loadingLeaderboard.SetActive(false);

        if (result.IsFailed)
        {
            failLeaderboard.SetActive(true);
        }
        else
        {
            GooglePlayManager.ActionLeaderboardsLoaded += ShowPlayerRank;
            GooglePlayInit.instance.ReloadLeaderboard();

            string leaderboardId = result.Leaderboard.Id;
            string msg = result.Message;
            Debug.Log("Score Submit Result for leaderboard " + leaderboardId + " :" + msg);
        }
    }

    private void ShowPlayerRank(GooglePlayResult result)
    {
        GPLeaderBoard leaderboard = GooglePlayManager.Instance.GetLeaderBoard("CgkIwtLMhJEUEAIQAA");

        GPScore currentPlayerScore = leaderboard.GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.GLOBAL);
        leaderboardText.text = string.Format("You took the place nº{0} on the leaderboard!", currentPlayerScore.Rank);
        successLeaderboard.SetActive(true);
    }

    public void ShareButton()
    {
        StartCoroutine(ShareScore());
    }

    public void RetryButton()
    {
        StartCoroutine(LoadSceneController.instance.LoadNextScene("Game"));
        Debug.Log("ASNDJNASDJ");
    }

    public void BackButton()
    {
        StartCoroutine(LoadSceneController.instance.LoadNextScene("Menu"));
    }

    IEnumerator ShareScore()
    {
        Application.CaptureScreenshot("share_screenshot");
        Texture2D shareTexture = new Texture2D(1280, 768, TextureFormat.RGB24, false);

        Debug.Log("Sharing");

        yield return new WaitForSeconds(0.5f);

        while (true)
        {
            WWW www = new WWW("file:///" + Application.persistentDataPath + "/" + "share_screenshot");
            yield return www;
            if (www.error == null)
            {
                Debug.Log("Texture Loading.");
                www.LoadImageIntoTexture(shareTexture);
                Debug.Log("Loaded");
            }
            else
            {
                Debug.Log(www.error);
            }

            break;
        }

        if (shareTexture != null)
        {
            Debug.Log("Sharing...");
            AndroidSocialGate.StartShareIntent("InfiniteRunner", "Look! I've got " + GameManager.instance.currentScore.ToString() + " on InfiniteRunner! This game is great! :)", shareTexture);
        }
        else
        {
            Debug.Log("Shared texture failed.");
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour {

    private float prevSpeed, prevTurning;

	void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
            GameManager.instance.EndGame();

        if(other.tag == "PlayerPredict")
        {
            prevSpeed = GameManager.instance.maxSpeed;
            prevTurning = GameManager.instance.maxTurning;
            GameManager.instance.maxTurning = GameManager.instance.maxTurning * 0.1f;
            GameManager.instance.maxSpeed = GameManager.instance.maxSpeed * 0.1f;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "PlayerPredict")
        {
            GameManager.instance.maxSpeed = prevSpeed;
            GameManager.instance.maxTurning = prevTurning;

            if(GameManager.instance.isRunning)
                StartCoroutine(NearMissRoutine());
        }
    }

    IEnumerator NearMissRoutine()
    {
        yield return new WaitForSeconds(0.2f);
        GameManager.instance.AddNearMiss();
    }
}

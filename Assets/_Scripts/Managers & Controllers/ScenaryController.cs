﻿using UnityEngine;
using System.Collections;

public class ScenaryController : MonoBehaviour {

    public GameObject forwardSpawn;

    void Update()
    {
        if(GameManager.instance.isRunning)
        {
            float turning = GameManager.instance.steerVelocity * GameManager.instance.maxTurning;
            float speed = GameManager.instance.maxSpeed;
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x + turning, 0, transform.position.z - speed), GameManager.instance.steerTime);

            if (transform.position.z < -30000)
                Destroy(gameObject);
        }
    }

    public void SpawnForward()
    {
        StartCoroutine(ScenaryManager.instance.SpawnNextScenary());
    }

    public void SpawnSides()
    {
        ScenaryManager.instance.SpawnSideScenary(gameObject);
    }
}

﻿using UnityEngine;
using System.Collections;

public class PredictDeath : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            Time.timeScale = 0.2f;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Obstacle")
        {
            Time.timeScale = 1f;
        }
    }

}

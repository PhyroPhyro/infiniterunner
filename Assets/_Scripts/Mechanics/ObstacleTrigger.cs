﻿using UnityEngine;
using System.Collections;

public class ObstacleTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
            transform.parent.GetComponent<Animator>().SetTrigger("ObstacleTrigger");
    }
}

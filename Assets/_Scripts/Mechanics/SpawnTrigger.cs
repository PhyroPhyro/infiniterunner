﻿using UnityEngine;
using System.Collections;

public class SpawnTrigger : MonoBehaviour {

    public enum SpawnTriggerTypes
    {
        Forward,
        Sides
    }

    public SpawnTriggerTypes spawnType;

	void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            switch(spawnType)
            {
                case SpawnTriggerTypes.Forward:
                    transform.parent.GetComponent<ScenaryController>().SpawnForward();
                    break;
                case SpawnTriggerTypes.Sides:
                    transform.parent.GetComponent<ScenaryController>().SpawnSides();
                    break;
            }

            Destroy(gameObject);
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GooglePlayInit : MonoBehaviour {

    public static GooglePlayInit instance;
    public GooglePlayerTemplate currentPlayer;
    public GPLeaderBoard leaderboard;

    void Start () {
        DontDestroyOnLoad(gameObject);
        instance = this;

        GooglePlayConnection.ActionConnectionResultReceived += ActionConnectionResultReceived;
        GooglePlayConnection.Instance.Connect();
        #if UNITY_EDITOR
        SceneManager.LoadSceneAsync("Menu");
        #endif
    }

    private void ActionConnectionResultReceived(GooglePlayConnectionResult result)
    {
        if (result.IsSuccess)
        {
            Debug.Log("Connected!");
            currentPlayer = GooglePlayManager.Instance.player;
            GooglePlayManager.ActionLeaderboardsLoaded += OnLeaderBoardsLoaded;
            GooglePlayManager.Instance.LoadLeaderBoards();
        }
        else
        {
            Debug.Log("Cnnection failed with code: " + result.code.ToString());
        }

        SceneManager.LoadSceneAsync("Menu");
    }

    public void ShowLeaderBoard()
    {
        GooglePlayManager.Instance.ShowLeaderBoardsUI();
    }

    public void ReloadLeaderboard()
    {
        GooglePlayManager.ActionLeaderboardsLoaded += OnLeaderBoardsLoaded;
        GooglePlayManager.Instance.LoadLeaderBoards();
    }

    private void OnLeaderBoardsLoaded(GooglePlayResult result)
    {
        GooglePlayManager.ActionLeaderboardsLoaded -= OnLeaderBoardsLoaded;
        if (result.IsSucceeded)
        {
            if (GooglePlayManager.Instance.GetLeaderBoard("CgkIwtLMhJEUEAIQAA") == null)
            {
                Debug.Log("Leader boards loaded," + "CgkIwtLMhJEUEAIQAA" + " not found in leader boards list");
                return;
            }

            leaderboard = GooglePlayManager.Instance.GetLeaderBoard("CgkIwtLMhJEUEAIQAA");
            long score = leaderboard.GetCurrentPlayerScore(GPBoardTimeSpan.ALL_TIME, GPCollectionType.GLOBAL).LongScore;

            Debug.Log(leaderboard.Name + " score");
        }
        else
        {
            AndroidMessage.Create("Leader-Boards Loaded error: ", result.Message);
        }
    }
}
